/*
printable
EolienneSavoDarrieus

copyright Cédric Doutriaux
2014

licence creativecommons cc-by-sa

*/



zstep=2.5;
_height=100;

/*
bearing 624ZZ
5X13
inside=4
*/


//#sphere(58);
//projection(cut = true)
//translate([0,0,-50])



rotate([0,0,$t*360]){

//base



difference(){
	union(){

		scale([1,1,0.3])
		sphere(16);
		translate([0,0,-5])
		cylinder(h=5,r=16);

		wings();

	}
	union(){
	translate([0,0,-6])
		cylinder(h=7,r=6.85);
		cylinder(h=95,r=2.3);
	}

}

//hat

difference(){
	
translate([0,0,_height])
scale([1,1,0.3])
sphere(13);
cylinder(h=99,r=2.3);
}


}


//////////////////////


module wings(){

color([0.8,0.4,0])
element();
rotate([0,0,120])
element();
rotate([0,0,240])
element();

}

module element(){
spire();
pale();
}


module spire(){

	for ( i = [0 : zstep :_height] )
	{
		hull(){
				palette(i);
				palette(i-zstep);
		
		}
	
	}

}

module palette(i){

rotate([0,0,(i*120/_height)])
		

			translate([(35.35*sin(i*180/_height)),(35.35*sin(i*180/_height)),i])

			rotate([0,0,(90*i/_height)])
			linear_extrude(height = zstep/10, center = false, convexity = 10, twist = -(120*zstep/_height))
			profil();
		



}



//savonius wing
module pale(){
linear_extrude(height = _height, center = false, convexity = 10, twist = -(120))
minkowski(){

	difference(){
		translate([-3,4,0])
		circle(8.3);
		translate([-2,6,0])
		circle(9.8);
	
	}
	circle(0.6);
}

}









module profil(){
translate([7,-7,0])
	hull(){
		circle(0.5);
		translate([0,14,0])
		scale([0.5,1,1])
		circle(3);
	}
}